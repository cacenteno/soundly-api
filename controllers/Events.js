const { Router } = require("express");
const express = require("express");
const router = express.Router();
const eventMiddleware = require("../middleware/Events");
const userMiddleware = require('../middleware/User');
const { route } = require("./User");
//Root Endpoint
router.get('/',eventMiddleware.getEvents(),async (req,res)=>{
    try {
       res.json({events: req.events}); 
    } catch (error) {
        throw error;
    }
});
router.get('/:_id', eventMiddleware.getSingleEvent(), async (req,res)=>{
    try{
        res.json({ event: req.event});
    }
    catch(err){
        throw err;
    }
});
router.post('/', userMiddleware.isAuthorized(),eventMiddleware.addEvent(), async (req,res)=>{
    try{
        res.json({ message: "Event successfully added."});
    }
    catch(err){
        throw err;
    }
});
router.patch('/', userMiddleware.isAuthorized(),eventMiddleware.editEvent(),async (req,res)=>{
    try {
      res.json({message: "Event successfully edited."});  
    } catch (error) {
        throw error;
    }
});
router.delete('/:_id',userMiddleware.isAuthorized(),eventMiddleware.deleteEvent(),async (req,res)=>{
    try {
        res.json({message: "Event successfully deleted."});  
    } catch (error) {
        throw error;
    }
});


module.exports = router;