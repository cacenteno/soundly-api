const express = require("express");
const path = require("path");
const router = express.Router();
const shortLinkMiddleware = require("../middleware/ShortLinks")

router.get("/", async (req,res)=>{
    res.sendFile(path.resolve('public/index.html'));
})
router.post('/', shortLinkMiddleware.addShortLink(),async (req,res)=>{
    try {
        res.json({message: req.link});
    } catch (error) {
        throw err;
    }
});
router.get('/:slug', shortLinkMiddleware.getShortLink(),async (req,res)=>{
    try {
        if(!req.link){
        res.redirect("http://192.168.0.17:3000")
        }
        else{
        res.redirect(req.link)
        }
    } catch (error) {
        throw err;
    }
});
router.post('',async (req,res)=>{
    try {
        res.json({});
    } catch (error) {
        
    }
});
router.patch('',async (req,res)=>{
    try {
        res.json({});
    } catch (error) {
        
    }
});
router.delete('/:_id', shortLinkMiddleware.deleteShortLink(),async (req,res)=>{
    try {
        res.json({ message: 'Shortlink Deleted.'});
    } catch (error) {
        throw err;
    }
});

module.exports = router;    