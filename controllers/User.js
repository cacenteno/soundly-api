const multer = require('multer');
const express = require("express");
const path = require("path");
const router = express.Router();
const UserMiddleWare = require("../middleware/User")

//Sets up location to save file and sets file name
const storage = multer.diskStorage({
  destination: "./public/uploads",
  filename: function (req, file, cb) {
    cb(null, 'avatar-' + Date.now() + path.extname(file.originalname))
  }
});

//Initializes Upload and sets the field to be found in FormData
const upload = multer({ storage });

//Root Endpoint
router.get("/", async (req,res)=>{
    res.json("Built by Centeq Solutions")
})
router.post('/profile', async (req,res)=>{
    try {
      res.json({message: "Profile updated!"});
    } catch (error) {
      throw err;
    }
}); 
router.post(
    "/signup", UserMiddleWare.doesExist(), UserMiddleWare.SignUp(),
    async (req, res, next) => { 
      try {
        res.json(`Thank you for signing up to Soundly. Check your email to verify your account.`)
      } catch (err) {
        throw err;
      }
    }
  );

 //Login Endpoint 
router.post("/login", UserMiddleWare.Login(),async (req, res, next) => {
  try {
  res.json({
    message: "Logged in",
    token: req.token,
    profile: req.profile
  })
  } catch (error) {
    error = new Error("Password incorrect!");
    throw error;
  }
});
module.exports = router;