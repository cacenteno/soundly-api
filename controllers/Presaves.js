const express = require("express");
const router = express.Router();
const presaveMiddleware = require("../middleware/Presaves");
const userMiddleware = require("../middleware/User")
router.get('/:_id', presaveMiddleware.getPresave(),async (req,res)=>{
    try {
       res.json({ presave: req.page});
    } catch (error) {
        throw error;
    }
});
router.post('/',userMiddleware.isAuthorized(), presaveMiddleware.addPresave(),async (req,res)=>{
    try {
        res.json({ message: "Presave Added!"});
    } catch (error) {
        throw error;
    }
});
router.patch('/:_id',userMiddleware.isAuthorized(), presaveMiddleware.editPresave(),async (req,res)=>{
    try {
        res.json({message: "Presave successfully edited!"});
    } catch (error) {
        throw error;
    }
});
router.delete('/:_id',userMiddleware.isAuthorized(), presaveMiddleware.deletePresave(),async (req,res)=>{
    try {
        res.json({
            message: "Presave successfully deleted!"
        });
    } catch (error) {
        throw err;
    }
});

module.exports = router;    