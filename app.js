/*
Title: Soundly.live
Description: Soundly.live is an application that makes music marketing easier.
Version: 1.0
Author: Cristian Centeno
*/
//Crucial imports, allows express to work, binds express to "app"
const express = require("express");
const app = express();
const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false);
const cors = require('cors');
//Controllers (routers) 
const userController = require("./controllers/User.js")
const eventControler = require("./controllers/Events");
const shortLinkController = require("./controllers/ShortLinks");
const presaveController = require("./controllers/Presaves")
// const releaseRoutes = require("./controllers/Releases.js")
// const artistRoutes = require("./controllers/Artists.js")
//Make app use bodyparser to interpret JSON and URL encoded URI's
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//Adds Cors Capabilities
app.use(cors());
//Includes env file to separate environment variables
require("dotenv").config();
//Use Public as a static folder 
app.use(express.static('public'))
//connect to mongo
var connect = mongoose.connect(process.env.MONGO_DB, {
  useNewUrlParser: true, useUnifiedTopology: true
}, console.log("Connected to db"));
//Routes
app.use("/", shortLinkController);
app.use("/api/presaves/", presaveController);
app.use("/api/", userController);
app.use("/api/events", eventControler)
// app.use("/api/artists", artistRoutes)
//Catch all Errors
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    error: err.message
  });
});
//Intialize Api
app.listen(3000, () => { console.log("Started") })
