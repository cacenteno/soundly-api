const User = require("../model/User");
const argon2 = require("argon2")
const mongoose = require('mongoose');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const path = require("path");
//Used to find if a user exists
function find(name, email, cb) {
  var thing = mongoose.connection.db.collection(name, function (err, collection) {
    collection.find({ email: email }).toArray(cb);
  })
  return thing;
}
module.exports = {
  doesExist: () => {
    return async (req,res,next)=>{
      try{
      var data = req.body;
      var user = await User.findOne({email: data.email.toLowerCase()}).exec();
      if(user){
        let err = new Error("User already exists!");
        next(err)
      }
      next(null,req)
      }
      catch(err){
        next(err)
      }
    }
  },
  //Restricts routes to only users that are logged in
  isAuthorized: () => {
    return async (req, res, next) => {
      try {
        let token = req.headers['secret-token']
        let valid = jwt.verify(token, process.env.SECRET)
        req.decoded = valid;
        next(null, req)
      }
      catch (error) {
        error = new Error("Please log in to view requested content. Need help ? Contact Cristian@ayakarecords.com")
        next(error);
      }
    }
  },
  isOwner: ()=>{
    return async (req,res,next)=>{
      try {
        switch(req.body.doc_type){
          case 'presave':
            
            next(null,req)
            break;
          case 'event':
            next(null,req)
            break;
          case 'shortlink':
            next(null,req)
            break;
          default:
            let err = new Error("Invalid document type. Please comeback soon.")
            next(err)
        }
      } catch (error) {
        throw error;
      }
    }
  },
  //Signs a user up
  SignUp: () => {
    return async (req, res, next) => {
      try {
          var accountData = req.body;
          //Hashes Password
          var hashedPassword;
          hashedPassword = await argon2.hash(req.body.password, { type: argon2.argon2id, memoryCost: 2 ** 16, hashLength: 50, }).then(hash => {
            return hash;
          })
          accountData.email = String(accountData.email).toLowerCase();
          accountData.password = hashedPassword;
          let userObj = new User(accountData);
          userObj.save();
          delete accountData.password;
          req.data = accountData;
          next(null, req)
      }
      catch (err) {
        next(err)
      }
    }
  },
  //Logs User and returns JWT to authorize session for restricted endpoints
  Login: () => {
    return async (req, res, next) => {
      try {
        //finds User
        const user = await find("users", String(req.body.email).toLowerCase(), async function (err, res) {
          //return error if not found 
          if (!res[0]) {
            let error = new Error("Account not found!");
            next(error)
            throw error;
          }
          //Their password
          var passwordEntered = req.body.password;
          //Password hashed password retrieved from db
          var passwordDatabase = res[0].password
          //Checks if passowrd is correct
          var isCorrect = await argon2.verify(passwordDatabase, passwordEntered).catch(err => { let error = new Error("Password Incorrect"); next(error); })
          //if not correct return Password Incorrect
          if (!isCorrect) {
            let error = new Error("Password Incorrect");
            next(error)
          }
          //if correct
          else if (isCorrect) {
            //Jwt Creation
            let data = {
              _id: res[0]._id,
              account_type: res[0].account_type,
            }
            const secret = process.env.SECRET;
            const expiration = '6h'
            //Create a token
            let token = jwt.sign({ data }, secret, { expiresIn: expiration });
            req.token = token;
            req.profile = res[0]
            //return token
            next(null, req)
          }
          else {
            let err = new Error("Password incorrect!");
            next(err)
          }
        });
      }
      catch (err) {
        let error = new Error("Password incorrect");
        next(error);
      }
    }
  }
}