const Event = require("../model/Event")
const User = require("../model/User")
const jwt = require('jsonwebtoken');
const ShortLink = require("../model/ShortLink");
function userID(req){
    let token = req.headers['secret-token'];
    let valid = jwt.verify(token, process.env.SECRET);
    return valid.data._id;
}
module.exports ={
    getEvents: ()=>{
        return async (req,res,next) =>{
            var events = await Event.find();
            req.events = events;
            next(null,req)
        }
    },
    getSingleEvent: ()=>{
        return async (req,res,next) =>{
            var event = await Event.findOne({slug: req.params._id});
            req.event = event
            next(null,req)
        }
    },
    addEvent: () =>{
        return async (req,res,next)=>{
            var event = req.body
            var uid = userID(req);
            event.owner = uid; 
            var eventObj = Event(event);
            var user = await User.findOneAndUpdate({_id: uid},{$push:{events:[eventObj]}})
            eventObj.save();
            next(null, req);
        }
    },
    editEvent: ()=>{
        return async (req,res,next)=>{
            var eventObject = await Event.findOneAndUpdate({_id: req.query._id},req.body);
            next(null,req)
        }
    },
    deleteEvent: ()=>{
        return async (req,res,next)=>{
            try {
                var uid = userID(req);
                var original = await Event.findOne({_id: req.params._id});
                if(original){
                var profile = await User.findOneAndUpdate({_id: uid},{$pull:{events: {_id: req.params._id}}})
                var eventObject = await Event.findOneAndDelete({_id: req.params._id}).exec();
                next(null,req)
                }
                else{
                    throw err;
                }
            } catch (error) {
                error = new Error("Event does not exist or you do not have acces to this resource. Contact support@soundly.live")
                next(error)
            }
        }
    }
}