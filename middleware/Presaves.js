const Presave = require("../model/Presave");
const Shortlink = require("../model/ShortLink");
const shortid = require('shortid');
const jwt = require('jsonwebtoken');
function userID(req){
    let token = req.headers['secret-token'];
    let valid = jwt.verify(token, process.env.SECRET);
    return valid.data._id;
}
module.exports = {
    addPresave: ()=>{
        return async (req,res,next)=>{
            var owner = userID(req);
            var presaveData = req.body;
            var presaveObj = new Presave(presaveData);
            presaveObj.owner = owner;
            presaveObj.save();
            var shortlinkObj = {
                title: presaveData.title,
                href: 'http://localhost:3000/api/presaves/' + presaveObj._id,
                owner: userID(req),
                slug: shortid.generate()
            }
            var shortlinkDoc = new Shortlink(shortlinkObj)
            shortlinkDoc.save();
            req.presave = presaveData;
            next(null, req);
        }
    },
    editPresave: ()=>{
        return async (req,res,next)=>{
            var presaveObj = await Presave.findOneAndUpdate({_id: req.params._id}, req.body);
            if(presaveObj){
                next(null, req)
            }
            else{
                let err = new Error("Presave does not exist!");
                next(err)
            }
            next(null, req)
        }
    },
    getPresave: ()=>{
        return async (req,res,next)=>{
            var presave = await Presave.findOneAndUpdate({_id: req.params._id},{$inc:{views: 1}});
            if(presave){
                req.page = presave;
                next(null,req)
            }
            else{
                let err = new Error("Presave does not exist!"); 
                next(err)
            }
        }
    },
    deletePresave: ()=>{
        return async (req,res,next)=>{
            var presave = await Presave.findOneAndDelete({_id: req.params._id});
            if(presave){
                next(null, req)
            }
            else{
                let err = new Error("Presave does not exist!");
                next(err)
            }
        }
    }
}