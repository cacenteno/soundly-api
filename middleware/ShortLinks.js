const ShortLink = require("../model/ShortLink");
const User = require("../model/User")
const validUrl = require('valid-url');
const shortid = require('shortid');
const jwt = require("jsonwebtoken")
function userID(req){
    let token = req.headers['secret-token'];
    let valid = jwt.verify(token, process.env.SECRET);
    return valid.data._id;
}
module.exports = {
    addShortLink:()=>{
        return async (req,res,next)=>{
            var uid = userID(req)
            var shortID = req.body.slug ? req.body.slug : shortid.generate();
            var newObj = {...req.body, owner: uid, slug: shortID};
            var shortLink = new ShortLink(newObj)
            shortLink.save()
            var user = await User.findOneAndUpdate({_id: uid},{$push:{short_links: shortLink}});
            req.link = shortLink;
            next(null,req)
        }
    },
    getShortLink:()=>{
        return async (req,res,next)=>{
            var shortLink = await ShortLink.findOne({slug: req.params.slug});
            if(shortLink){
                req.link = shortLink.href
                next(null,req)
            }
            else{
                let err = Error("Shortlink not found!")
                next(err)
            }
        }
    },
    editShortLink:()=>{
        return async (req,res,next)=>{

            next(null,req)
        }
    },
    deleteShortLink:()=>{
        //Make sure to remove from profile
        return async (req,res,next)=>{
            var uid = userID(req)
            var original = await ShortLink.findOne({owner: uid});
            if(original && original.owner == uid){
            var shortlink, profile;
            profile = await User.findOneAndUpdate({_id: uid},{$pull:{short_links: {_id: req.params._id}}})
            shortlink = await ShortLink.findOneAndDelete({_id: req.params._id});
            next(null,req)
            }
            else{
                let err = new Error('You do not have the permissions to do this.')
                next(err)
            }
        }
    },
}