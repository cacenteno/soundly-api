const mongoose = require("mongoose")
const Schema = mongoose.Schema;

mongoose.set("useCreateIndex", true);

const Presave = new Schema({
        title: String,
        presave_type: String,
        release_date: Date,
        album_title: String,
        genre: String,
        tracks: Array,
        artists: Array,
        analytics_tags: Array,
        slug: String,
        page_styles: Object,
        owner: String,
        views: Number
})
module.exports = mongoose.model('presaves', Presave);