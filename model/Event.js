const mongoose = require("mongoose")
const Schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const Event = new Schema({
    thumbnails: Array,
    title: String,
    artists: Array,
    age_restristion: String,
    ticket_links: Array,
    description: String,
    organizer_socials: Array,
    price: Number,
    owner: String
})
module.exports = mongoose.model('events', Event);