const mongoose = require("mongoose")
const Schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const Release = new Schema({
album_art:{
    type: Array
},
title:{
    type: String
},
artist: {
    type: String
},
year:{
    type: String
},
album: {    
    type: String
},
about:{
    type: String
},
spotify_url:{
    type: String
},
save_track: {
    type: String
},
featured:{
    type: Boolean
},
new_release:{
    type: Boolean
},
tracks:{
    type: Array
},
release_type:{
    type: String
},
released: {
    type: Boolean
},
seo_name:{
    type: String
}
})
module.exports = mongoose.model('releases', Release);