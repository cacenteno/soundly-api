const mongoose = require("mongoose")
const Schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

/*
email,password,photos,verified,connections,location,phone_num,carrier,sms_email,rating
Discriminator: account_type
*/
const shortLinkSubDoc = new Schema({
    title: String,
    href: String,
    slug: String
})
const eventSubDoc = new Schema({
    title: String,
    artists: Array,
    age_restriction: String,
    ticket_links: Array,
    description: String,
    organizer_socials: Array
})
// const presaveSubDoc = new Schema({

// })
const User = new Schema({
avatar: {
    type: Array
},
first_name:{
    type: String
},
last_name:{
    type: String
},
email:{
    type: String,
    required: true,
    unique: true
},
password:{
    type: String,
    required: true
},
presaves:{
    type: Array
},
banned:{
    type: Boolean
},
city:{
    type: String
},
state:{
    type: String
},
external_links: {
    type: Array
},
short_links: [shortLinkSubDoc],
events: [eventSubDoc],
page_styles:{
    type: Object
},
role: String
})
module.exports = mongoose.model('users', User);