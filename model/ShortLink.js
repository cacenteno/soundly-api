const mongoose = require("mongoose")
const Schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const ShortLink = new Schema({
    title: String,
    slug: String,
    href: String,
    owner: String,
    visited: Number
})
module.exports = mongoose.model('short_links', ShortLink);